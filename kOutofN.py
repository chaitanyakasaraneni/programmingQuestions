"""
Question:
    Choose k balls out of n balls (e.g., balls 0,1,2), 
    populate all combinations, e.g., for k=2, n=3, outcomes: [0,1], [0,2] and     [1,2]; 


  public static void main (String[] args) { 
     System.out.println(findAllCombinations(2,3)); // output should be [[0, 1], [0, 2], [1, 2]]
     System.out.println(findAllCombinations(3,5)); // [[0, 1, 2], [0, 1, 3], [0, 1, 4], [0, 2, 3], [0, 2, 4], [0, 3, 4], [1, 2, 3], [1, 2, 4], [1, 3, 4], [2, 3, 4]]

  }
  }
"""

def choose_iter(elements, length):
    for i in range(len(elements)):
        if length == 1:
            yield (elements[i],)
        else:
            for next in choose_iter(elements[i+1:len(elements)], length-1):
                yield (elements[i],) + next

def findAllUniqueCombiations(k=2,n=3):
    if k == 0 or n == 0 or k > n:
        return
    arr = [i for i in range(n)]
    return list(choose_iter(arr, k))
    
if __name__=='__main__':
    print(findAllUniqueCombiations(3,7))
