"""
Given an array arr[] of integers, find out the maximum difference between any two elements such that larger element appears after the smaller number. 

Input : arr = [2, 3, 10, 6, 4, 8, 1]
Output : 8
Explanation : The maximum difference is between 10 and 2.

Input : arr = {7, 9, 5, 6, 3, 2}
Output : 2
Explanation : The maximum difference is between 9 and 7.

"""
def maxDiff(arr, arr_size): 
    max_diff = arr[1] - arr[0] 
    min_element = arr[0] 
      
    for i in range( 1, arr_size ): 
        if (arr[i] - min_element > max_diff): 
            max_diff = arr[i] - min_element 
        if (arr[i] < min_element): 
            min_element = arr[i] 
    return max_diff


if __name__=="__main__":
    arr_size = int(input())
    arr = input()
    arr = [int(x) for x in arr.split()]
    print(maxDiff(arr, arr_size))
