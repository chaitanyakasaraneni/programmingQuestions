### Question

Davy has buried pieces of his ancient artifacts underground. The artifacts are hidden in an area split into an NxN grid of square cells. 
The rows are numbered from 1 to N. The columns are labeled with consecutive English upper-case letters (A, B, C, etc.). Each cell is identified by a string composed of its row number followed by its column number: 
for example, "9C" denotes the third cell in the 9th row, and "150" denotes the fourth cell in the 15th row.

Jack was out looking for the artifact pieces in this area. He chose some map cells in which to search for artifact pieces. He is able to reconstruct an artifact if he found all of the pieces of an artifact. The goal is to count the number of artifacts for which he has found all of the pieces, as well the number of artifacts for which he's found some but not all of the pieces.

