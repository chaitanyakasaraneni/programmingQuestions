def searchArtifacts(n, artifacts, searched):
    '''
    Function to calculate the total artifacts reconstructed and partial items found
    INPUT :
        n - integer
        artifacts - string with location of artifacts
        searched - string with coordinates searched
        
    OUTPUT :
        a List containing count of artifacts reconstructed and count of artifacts for which some but not all parts were found
    '''
    # Check if input is not empty
    if n == 0 or len(artifacts) == 0 or artifacts == None or len(searched) == 0 or artifacts == None or artifacts == " " or searched == " ":
        return [0,0]
    
    # Result Variables
    compCount = 0
    partialCount = 0
    
    artifacts = artifacts.split(',')
        
    searchedList = searched.split(' ')
    
    # Empty matrix creation
    location = [[0 for x in range(n)] for y in range(n)]
    
    # Dealing with searched list
    for sear_pos in searchedList:
        if sear_pos == None or len(sear_pos) == 0: continue
        row = int(sear_pos[:-1]) - 1 # the range of matrix is 0 ~ N-1
        col = ord(sear_pos[-1]) - ord('A')
        location[row][col] = 1 # mark the cell as searched in the location matrix
        
    # Dealing with artifacts location
    for item in artifacts:
        if item == None or len(item) == 0: continue
        
        position = item.split(' ')
        if position == None or len(position) == 0: continue
        
        # Positions of items in matrix
        topLeft = position[0]
        bottomRight = position[1]
        
        # Starting and ending positions of rows and columns
        rowStart = int(topLeft[:-1]) - 1 # the range of location matrix is 0 ~ N-1
        colStart = ord(topLeft[-1]) - ord('A')
        rowEnd = int(bottomRight[:-1]) - 1 # the range of location matrix is 0 ~ N-1
        colEnd = ord(bottomRight[-1]) - ord('A')
        
        # Identify if the position of artifact's pieces is searched
        posSearch = False
        posNotSearch = False
    
        # Checking the searched location matrix against artifact matrix
        for rowIndex in range(rowStart, rowEnd+1):
            for colIndex in range(colStart, colEnd+1):
                if location[rowIndex][colIndex] == 1:
                    posSearch = True
                else:
                    posNotSearch = True
        
        if posSearch == True and posNotSearch == False: compCount += 1
        if posSearch == True and posNotSearch == True: partialCount += 1
        
    return [compCount, partialCount]
    
    
if __name__ == '__main__':
    res = searchArtifacts(4, "1B 2C,2D 4D", "2B 2D 3D 4D 4A")
    print(res)
    res = searchArtifacts(3, "1A 1B,2C 2C", "1B")
    print(res)
    res = searchArtifacts(12, "1A 2A,12A 12A", "12A")
    print(res)
    res = searchArtifacts(4, "", "")
    print(res)
    res = searchArtifacts(1, " ", " ")
    print(res)
    res = searchArtifacts(1, "", " ")
    print(res)
