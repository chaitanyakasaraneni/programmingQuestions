"""
Arrange meetings within next 'k' hours for a meeting room. Each meeting has a duration 'd' and priority 'p' (0<=p<=100). Calculate the max sum of priority values for meetings that could be arranged in 'k' hours

Test Case-1:
Input:
k = 3
meetings = [[1,100],[2,50],[5,100],[1,60]]

Output:
160

Test Case-2:
Input:
k = 5
meetings = [[1,100],[2,10],[1,50],[2,20],[3,100]]

"""

def maxScore(k, meetings):
    if k<=0:
        return 0
    n = len(meetings)
    score = [[0]*(k+1) for _ in range(n+1)]
    for i in range(1, k+1):
        for j in range(1, n+1):
            score[j][i] = score[j-1][i]
            if meetings[j-1][0] <= k:
                score[j][i] = max(score[j][i], meetings[j-1][1] +
                                  score[j-1][i-meetings[j-1][0]])
    return score[n][k]

if __name__=="__main__":
    print(maxScore(3, [[1,100],[2,50],[5,100],[1,60]]))
