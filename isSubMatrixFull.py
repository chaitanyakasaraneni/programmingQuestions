
# is submatrix full

'''
You are given numbers, a 3 × n matrix which contains only digits from 1 to 9. Consider a sliding window of size 3 × 3, which is sliding from left to right through the matrix numbers. The sliding window has n - 2 positions when sliding through the initial matrix.

Your task is to find whether or not each sliding window position contains all the numbers from 1 to 9 (inclusive). Return an array of length n - 2, where the ith element is true if the ith state of the sliding window contains all the numbers from 1 to 9, and false otherwise.

Example

numbers = [[1, 2, 3, 2, 5, 7],
           [4, 5, 6, 1, 7, 6],
           [7, 8, 9, 4, 8, 3]]

the output should be isSubmatrixFull(numbers) = [true, false, true, false].
Let's consider all sliding window states:

    The 1st state contains all digits from 1 to 9.
    The 2nd state doesn't contain digit 7.
    The 3rd state contains all digits from 1 to 9.
    The 4th state doesn't contain digit 9.
    Summary, there are four states of the sliding window, and the resulted array is [true, false, true, false].
'''

""" Solution 1 """

def calcSum(rs, re, cs, ce, matrix):
    sums = 0
    print(rs,re,cs,ce)
    for i in range(rs,re):
        for j in range(cs, ce):
            sums += matrix[i][j]
            
    if sums == (9*10)/2:
        return True
    else:
        return False
    
    

def isSubMatrixFull(matrix):
    
    res = []
    rowLen = len(matrix)
    colLen = len(matrix[0])
    
    for i in range(0,rowLen-2):
        for j in range(0,colLen-2):
            res.append(calcSum(i, i + 3, j, j + 3, matrix))
    
    print(res)

    
""" Solution 2 """
def getCol(mat, col):
    return [mat[i][col] for i in range(3)]

def isSubMatrixFull(mat):
    n = len(mat[0])
    ans = [False]*(n-2)
    kernel = getCol(mat, 0) + getCol(mat, 1) + getCol(mat, 2) # O(1)
    for i in range(n - 2): # O(n)
        if len(set(kernel)) == 9: # O(1)
            ans[i] = True # O(1)
        if i < n - 3: # O(1)
            kernel = kernel[3:] + getCol(mat, i + 3) # O(1)
    return ans


    
if __name__ == '__main__':
    
    isSubMatrixFull([[1,2,3,2,5,7],[4,5,6,1,7,6],[7,8,9,4,8,3]])
    isSubMatrixFull([[1,2,3,2],[4,5,6,1],[7,8,9,4]])
    isSubMatrixFull([[1,2,3,2,5,7,1],[4,5,6,1,7,6,2],[7,8,9,4,8,3,9]])
    isSubMatrixFull([[1,2,3,2,5,7,1],[4,5,6,1,7,6,2],[7,8,9,4,8,3,9],[7,8,9,4,8,3,9],[7,8,9,4,8,3,9]])
    
    
