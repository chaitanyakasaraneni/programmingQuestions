import sys
class solution:
    def moonsAndUmbrellas(self, cases):
        for pos, case in enumerate(cases):
            cost_x = case[0]
            cost_y = case[1]
            case_str = case[2]
            res = [[0]*len(case_str) for _ in range(2)]
            if case_str[0] == 'C':
                res[1][0] = sys.maxsize
            elif case_str[0] == "J":
                res[0][0] = sys.maxsize
            for idx in range(1, len(case_str)):
                if case_str[idx] == "C":
                    res[1][idx] = sys.maxsize
                    res[0][idx] = min(res[0][idx-1], res[1][idx-1] + cost_y)
                elif case_str[idx] == "J":
                    res[0][idx] = sys.maxsize
                    res[1][idx] = min(res[0][idx-1] + cost_x, res[1][idx-1])
                elif case_str[idx] == "?":
                    res[0][idx] = min(res[0][idx-1], res[1][idx-1] + cost_y)
                    res[1][idx] = min(res[0][idx-1] + cost_x, res[1][idx-1])
            print(f"Case #{pos + 1}: {min(res[0][len(case_str)-1], res[1][len(case_str)-1])}")

if __name__ == "__main__":
    num_cases = int(input())
    cases = []
    for num in range(num_cases):
        case = input().split(' ')
        cases.append([int(case[0]), int(case[1]), case[2]])
    sol = solution()
    sol.moonsAndUmbrellas(cases)