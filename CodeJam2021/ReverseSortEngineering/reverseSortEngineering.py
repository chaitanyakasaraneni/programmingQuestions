class solution:
    def reverseSortEngineering(self, cases):
        for pos, case in enumerate(cases):
            arr_len = case[0]
            arr_cost = case[1]
            if arr_cost < arr_len - 1 or arr_cost > arr_len*(arr_len+1)/2 - 1:
                print(f'Case #{pos+1}: IMPOSSIBLE')
            else:
                arr = []
                resultArr = list(range(1, arr_len+1))
                for i in range(arr_len-1):
                    remaining = arr_len - 2 - i
                    if arr_cost - remaining <= arr_len - i:
                        maxCost = arr_cost-remaining
                        arr_cost -= maxCost
                    else:
                        maxCost = arr_len-i
                        arr_cost-=maxCost
                    arr.append(maxCost)
                arr.reverse()
                for i in range(arr_len-1):
                    if arr[i]!=1:
                        resultArr[arr_len-i-2:arr_len-i-2+arr[i]] = resultArr[arr_len-i-2:arr_len-i-2+arr[i]][::-1]
                print(f'Case #{pos+1}: {" ".join(map(str, resultArr))}')

if __name__ == "__main__":
    num_cases = int(input())
    cases = []
    for num in range(num_cases):
        line = input().split(' ')
        case_len = int(line[0])
        cost = int(line[1])
        cases.append([case_len, cost])
    sol = solution()
    sol.reverseSortEngineering(cases)