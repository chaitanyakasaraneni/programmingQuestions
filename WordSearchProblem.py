"""
Word Search Problem : Write a Python or Java Program

You are given a grid of n*n letters, followed by m number of words. The words may occur anywhere in the grid in a row or a column, forward or backward. There are no diagonal words, however.

Print space separated strings "Yes" if the word is present and "No" if it isn't.

Input 0:
3
C A T
I D O
M O M
5
CAT TOM ADO MOM CDM

OUTPUT:
Yes Yes Yes Yes No

Input 1:
3
M O M
S O N
R A T
4
MNT MSR OOB

OUTPUT:
Yes Yes No
"""

n=int(input())
arr=[0]*n;

for i in range(0,n):
   temp=input().split(" ")
   arr[i]=[0]*n
   for j in range(0,n):
       arr[i][j]=temp[j]

m=int(input())
key=[""]*m
temp=input().split(" ")
for i in range(0,len(temp)):
   key[i]=temp[i]

for i in range(0,len(temp)):
   isFound=False
   for j in range(0,n):
       temp1=""
       temp2=""
       for k in range(0,n):
           temp1=temp1+arr[k][j]
           temp2=temp2+arr[j][k]
       if(temp1==key[i] or (temp1[::-1])==key[i] or temp2==key[i] or (temp2[::-1])==key[i]):
           print("Yes",end=" ")
           isFound=True
   if(isFound==False):
       print("No",end=" ")

print()
