class RandomizedCollection(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.values, self.indexes = [], collections.defaultdict(set)

    def insert(self, val):
        """
        Inserts a value to the collection. Returns true if the collection did not already contain the specified element.
        :type val: int
        :rtype: bool
        """
        self.values.append(val)
        self.indexes[val].add(len(self.values)-1)
        return len(self.indexes[val]) == 1
    

    def remove(self, val):
        """
        Removes a value from the collection. Returns true if the collection contained the specified element.
        :type val: int
        :rtype: bool
        """
        if not self.indexes[val]:
            return False
        x = self.indexes[val].pop()
        self.values[x] = None
        return True

    def getRandom(self):
        """
        Get a random element from the collection.
        :rtype: int
        """
        x = None
        while x is None:
            x = random.choice(self.values)
        return x


# Your RandomizedCollection object will be instantiated and called as such:
# obj = RandomizedCollection()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()
